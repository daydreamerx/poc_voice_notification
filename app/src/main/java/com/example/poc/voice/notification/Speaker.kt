package com.example.poc.voice.notification

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import android.util.Log
import java.util.*


class Speaker(context: Context?) : OnInitListener {
    private var tts: TextToSpeech = TextToSpeech(context, this,  "com.google.android.tts")
    private var ready = false
    private var allowed = false

    companion object {
        const val SPEED_SLOW = "SLOW"
        const val SPEED_NORMAL = "NORMAL"
        const val SPEED_FAST = "FAST"
    }

    fun isAllowed(): Boolean {
        return allowed
    }

    fun allow(allowed: Boolean) {
        this.allowed = allowed
    }

    override fun onInit(status: Int) {
        Log.e("XXX", "TextToSpeech.SUCCESS $status")
        if (status == TextToSpeech.SUCCESS) {
            // Change this to match your
            // locale
            tts.language = Locale.forLanguageTag("th")
            ready = true
        } else {
            ready = false
        }
    }

    fun speak(text: String?) {
        // Speak only if the TTS is ready
        // and the user has allowed speech
        Log.e("XXXY", "ready $ready allow $allowed")
//        if (ready && allowed) {
            tts.speak(text, TextToSpeech.QUEUE_ADD, null, null)
//        }
    }

    fun pause(duration: Int) {
        tts.playSilence(duration.toLong(), TextToSpeech.QUEUE_ADD, null)
    }

    fun speed(level: String) {
        when(level) {
            SPEED_SLOW -> {
                tts.setSpeechRate(0.5f)
            }
            SPEED_NORMAL -> {
                tts.setSpeechRate(1f)
            }
            else -> {
            tts.setSpeechRate(1.5f)
            }
        }
    }

    // Free up resources
    fun destroy() {
        tts.shutdown()
    }
}