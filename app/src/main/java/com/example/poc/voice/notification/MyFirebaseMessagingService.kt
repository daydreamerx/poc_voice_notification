package com.example.poc.voice.notification

import android.Manifest
import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.poc.voice.notification.Speaker.Companion.SPEED_FAST
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*


class MyFirebaseMessagingService : FirebaseMessagingService() {
    var screenCheck: String = ""
    var bookingNo: String = ""
    var transactionRefId: String = ""
    private var speaker: Speaker? = null
    var bodyMessage: String = ""

    companion object {
        const val SCREEN_REDIRECT = "screen"
        const val TRANSACTION_REF_ID = "transactionRefId"
        const val BOOKING_NO = "bookingNo"
        const val CURRENT = "Current"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e("","From: ${remoteMessage.from}")
        Log.e("","From Data : ${remoteMessage.data}")

        screenCheck = remoteMessage.let {
            it.data[SCREEN_REDIRECT]
        } ?: ""

        bookingNo = remoteMessage.let {
            it.data[BOOKING_NO]
        } ?: ""

        transactionRefId = remoteMessage.let {
            it.data[TRANSACTION_REF_ID]
        } ?: ""

        remoteMessage.notification?.let {
            Log.e("","Message notification payload: title -> ${it.title} body -> ${it.body} screen -> $screenCheck")
            createNotification(
                mapToData(
                    it.title ?: "",
                    it.body ?: "",
                    screenCheck
                )
            )
        }
    }

    override fun onNewToken(token: String) {
        registerFirebaseToken(token)
    }

    private fun registerFirebaseToken(token: String) {
            val saveToken = NotificationTokenRequestModel(
                token = token,
                tokenType = "FCM"
            )
    }

    private fun mapToData(title: String, mes: String, screen: String): NotificationData {
        return NotificationData(
            title = title,
            message = mes,
            screen = screen
        )
    }

    private fun createNotification(notiData: NotificationData) {
        val pendingIntent = PendingIntent.getActivity(
            this,
            getNotificationId(),
            Intent(),
            PendingIntent.FLAG_IMMUTABLE
        )

        val builder = getNotificationBuilder(notiData = notiData, pendingIntent = pendingIntent)
        setNotificationManager(builder = builder)
    }

    private fun getNotificationId() :Int {
        return (Calendar.getInstance().timeInMillis % 10000000).toInt()
    }

    private fun getPendingIntent(intent: Intent?): PendingIntent {
        return PendingIntent.getActivity(
            this,
            getNotificationId(),
            intent,
            PendingIntent.FLAG_IMMUTABLE
        )
    }

    private fun getNotificationBuilder(notiData: NotificationData, pendingIntent: PendingIntent): NotificationCompat.Builder {
//        val channelId = "tungngern_02"
        val channelId = getString(R.string.default_notification_channel_id)
        val largeIcon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)
        return NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(largeIcon)
            .setContentTitle(notiData.title)
            .setContentText(notiData.message)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(NotificationCompat.DEFAULT_LIGHTS or NotificationCompat.DEFAULT_VIBRATE)
            .setAutoCancel(true)
            .setStyle(NotificationCompat.BigTextStyle().bigText(notiData.message))
    }

    private fun setNotificationManager(builder: NotificationCompat.Builder) {
//        val channelId = "tungngern_02"
        val channelId = getString(R.string.default_notification_channel_id)
        val sound = Uri.parse((ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this@MyFirebaseMessagingService.packageName) + "/" + R.raw.a_sound) //Here is FILE_NAME is the name of file that you want to play
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, "Default", importance).apply {
                setShowBadge(false)
                enableVibration(true)
                setSound(sound, attributes)
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        with(NotificationManagerCompat.from(applicationContext)) {
            if (ActivityCompat.checkSelfPermission(
                    this@MyFirebaseMessagingService,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            notify(getNotificationId(), builder.build())
            speaker?.pause(3000)
            speaker?.speed(SPEED_FAST)
            speaker?.speak(bodyMessage)
        }
    }

    override fun handleIntent(intent: Intent) {
        speaker = Speaker(this@MyFirebaseMessagingService)
        val bundle: Bundle? = intent.extras
        val screen = bundle?.getString("screen")
        val title = bundle?.getString("gcm.notification.title")
        val message = bundle?.getString("gcm.notification.body")
        val notiData = NotificationData(title = title, message = message, screen = screen)
        bodyMessage = message.toString()
        if (screen == CURRENT && isInBackground()) {

            val launchIntent = packageManager.getLaunchIntentForPackage(this.packageName)
            launchIntent?.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

            val pendingIntent = getPendingIntent(launchIntent)
            val builder = getNotificationBuilder(notiData = notiData, pendingIntent = pendingIntent)
            setNotificationManager(builder)
        } else {
            super.handleIntent(intent)
        }
    }

    private fun isInBackground(): Boolean {
        val myProcess = ActivityManager.RunningAppProcessInfo()
        ActivityManager.getMyMemoryState(myProcess)
        return myProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
    }


    class NotificationData(
        val title: String? = "",
        val message: String? = "",
        val screen: String? = ""
    )

    data class NotificationTokenRequestModel(
        val token: String,
        val tokenType: String
    )
}
